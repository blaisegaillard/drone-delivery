package com.blaise.dronedelivery;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinatesTest {

    @Test
    void distance() {

        Coordinates coordinates1 = new Coordinates("N1E1");
        Coordinates coordinates2 = new Coordinates("N1E2");

        // Same zone
        double distance1 = Coordinates.distance(coordinates1, coordinates2);
        assertEquals(1, distance1);

        Coordinates coordinates3 = new Coordinates("S2E1");

        // One N one S
        double distance2 = Coordinates.distance(coordinates1, coordinates3);
        assertEquals(3, distance2);


        // One E one W
        Coordinates coordinates4 = new Coordinates("N11W5");
        Coordinates coordinates5 = new Coordinates("N11E5");
        double distance3 = Coordinates.distance(coordinates4, coordinates5);
        assertEquals(10, distance3);

    }
}