package com.blaise.dronedelivery;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RouteTest {

    @Test
    void calculateNPS() {
        Order order1 = new Order("WM001 N11W5 05:11:50");
        Order order2 = new Order("WM002 S3E2 05:11:55");
        Order order3 = new Order("WM003 N7E50 01:31:50");
        Order order4 = new Order("WM004 N11E5 04:11:50");

        // Positive
        Delivery delivery1 = new Delivery();
        delivery1.setOrder(order2);
        delivery1.setDeliveryTime(LocalTime.parse("06:00:00"));

        // Positive
        Delivery delivery2 = new Delivery();
        delivery2.setOrder(order1);
        delivery2.setDeliveryTime(LocalTime.parse("06:07:13"));

        // Negative
        Delivery delivery3 = new Delivery();
        delivery3.setOrder(order4);
        delivery3.setDeliveryTime(LocalTime.parse("06:31:24"));

        // Neutral
        Delivery delivery4 = new Delivery();
        delivery4.setOrder(order3);
        delivery4.setDeliveryTime(LocalTime.parse("06:55:35"));

        ArrayList<Delivery> deliveries = new ArrayList<>();
        deliveries.add(delivery1);
        deliveries.add(delivery2);
        deliveries.add(delivery3);
        deliveries.add(delivery4);

        Route route = new Route();
        route.setDeliveries(deliveries);

        // 50% positive - 25% negative
        assertEquals(25, route.calculateNPS());

    }

}