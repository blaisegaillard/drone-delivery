package com.blaise.dronedelivery;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class DeliveryTest {

    @Test
    void calculateNPS() {

        Order order1 = new Order("WM001 N1E1 05:11:50");

        // Promoter
        Delivery delivery1 = new Delivery();
        delivery1.setOrder(order1);
        delivery1.setDeliveryTime(LocalTime.parse("06:20:21"));

        assertEquals(1, delivery1.calculateNPSContribution());

        // Detractor
        Delivery delivery2 = new Delivery();
        delivery2.setOrder(order1);
        delivery2.setDeliveryTime(LocalTime.parse("15:20:21"));

        assertEquals(-1, delivery2.calculateNPSContribution());

        // Neutral
        Delivery delivery3 = new Delivery();
        delivery3.setOrder(order1);
        delivery3.setDeliveryTime(LocalTime.parse("08:20:21"));

        assertEquals(0, delivery3.calculateNPSContribution());

    }
}