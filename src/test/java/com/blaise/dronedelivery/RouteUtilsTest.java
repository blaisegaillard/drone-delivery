package com.blaise.dronedelivery;

import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RouteUtilsTest {

    @Test
    void findOptimalRoute() {
        List<Order> orders = new ArrayList<>();
        Order order1 = new Order("WM001 N11W5 05:11:50");
        orders.add(order1);
        Order order2 = new Order("WM002 S23E22 05:11:55");
        orders.add(order2);
        Order order3 = new Order("WM003 N7E50 05:31:50");
        orders.add(order3);
        Order order4 = new Order("WM004 N11E5 04:09:50");
        orders.add(order4);

        Route optimalRoute = RouteUtils.findOptimalRoute(orders);

        assertEquals(75, optimalRoute.getNPS().intValue());

    }


}