package com.blaise.dronedelivery;

public class Coordinates {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public Coordinates(String coordinateString) {

        int indexOfEW = Math.max(coordinateString.indexOf('E'), coordinateString.indexOf('W'));

        String xString = coordinateString.substring(0, indexOfEW);
        String yString = coordinateString.substring(indexOfEW);

        int xAbs = Integer.parseInt(xString.substring(1));
        x = xString.charAt(0) == 'S' ?  -1 * xAbs : xAbs;

        int yAbs = Integer.parseInt(yString.substring(1));
        y = yString.charAt(0) == 'W' ?  -1 * yAbs : yAbs;

    }

    public static double distance(Coordinates start, Coordinates end) {
        return Math.sqrt(
                (end.getY() - start.getY()) * (end.getY() - start.getY()) +
                (end.getX() - start.getX()) * (end.getX() - start.getX())
        );
    }

}
