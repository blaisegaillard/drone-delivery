package com.blaise.dronedelivery;

import java.time.LocalTime;

public class Delivery {

    private Order order;
    private LocalTime deliveryTime;

    public Delivery() {
    }

    public Delivery(Order nextOrder, Delivery previousDelivery) {
        order = nextOrder;
        deliveryTime = calculateArrivalTime(previousDelivery.getOrder().getCoordinates(), nextOrder.getCoordinates(), previousDelivery.getDeliveryTime());
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public LocalTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public int calculateNPSContribution() {
        int deliveryHours = (getDeliveryTime().toSecondOfDay() - getOrder().getOrderTime().toSecondOfDay()) / 3600;
        if (deliveryHours >= 4) {
            return -1;
        }
        if (deliveryHours <= 1) {
            return 1;
        }
        return 0;
    }

    public static LocalTime calculateArrivalTime(Coordinates start, Coordinates end, LocalTime startTime) {
        return startTime.plusSeconds(Math.round(Coordinates.distance(start, end) * 60));
    }
}
