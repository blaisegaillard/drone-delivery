package com.blaise.dronedelivery;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static com.blaise.dronedelivery.Delivery.calculateArrivalTime;

public class Route {

    private List<Delivery> deliveries;
    private Integer NPS;

    public Route() {
        this.deliveries = new ArrayList<>();
    }

    public Route(List<Order> orders) {
        deliveries = new ArrayList<>();

        if (orders != null && !orders.isEmpty()) {
            Order firstOrder = orders.get(0);
            LocalTime firstDeliveryTime = firstOrder.getOrderTime().isBefore(LocalTime.parse("06:00:00")) ? LocalTime.parse("06:00:00") : firstOrder.getOrderTime();
            Delivery firstDelivery = new Delivery();
            firstDelivery.setDeliveryTime(firstDeliveryTime);
            firstDelivery.setOrder(firstOrder);
            deliveries.add(firstDelivery);

            Delivery previousDelivery = firstDelivery;
            for (int i = 1; i <= orders.size() - 1; i++) {
                Order order = orders.get(i);
                Delivery delivery = new Delivery();
                LocalTime deliveryTime = calculateArrivalTime(previousDelivery.getOrder().getCoordinates(), order.getCoordinates(), previousDelivery.getDeliveryTime());
                if (deliveryTime.isBefore(order.getOrderTime())) {
                    deliveryTime = order.getOrderTime();
                }
                delivery.setDeliveryTime(deliveryTime);
                delivery.setOrder(order);
                deliveries.add(delivery);
                previousDelivery = delivery;
            }
        }
        NPS = calculateNPS();

    }

    public List<Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public Integer getNPS() {
        return NPS;
    }

    public void setNPS(Integer NPS) {
        this.NPS = NPS;
    }

    public int calculateNPS() {
        int numberDeliveries = deliveries.size();
        int rawNPS = 0;
        for (Delivery delivery : deliveries) {
            rawNPS += delivery.calculateNPSContribution();
        }
        return rawNPS * 100 / numberDeliveries;
    }
}
