package com.blaise.dronedelivery;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        Path inputFile = Paths.get(args[0]);
        List<Order> orders = Files.readAllLines(inputFile).stream()
                .map(Order::new)
                .collect(Collectors.toList());

        Path outputFile = inputFile.getParent().resolve("output.txt");

        Route optimalRoute = RouteUtils.findOptimalRoute(orders);

        List<String> outputLines = new ArrayList<>();
        for (Delivery delivery : optimalRoute.getDeliveries()) {
            outputLines.add(delivery.getOrder().getId() + " " + delivery.getDeliveryTime().toString());
        }
        outputLines.add("NPS " + optimalRoute.calculateNPS());

        Files.write(outputFile, outputLines);

        System.out.println("Output file: " + outputFile.toUri());

    }
}
