package com.blaise.dronedelivery;

import java.time.LocalTime;

public class Order {

    private String id;
    private Coordinates coordinates;
    private LocalTime orderTime;

    public Order(String orderString) {
        String[] details = orderString.split(" ");

        id = details[0];
        coordinates = new Coordinates(details[1]);
        orderTime = LocalTime.parse(details[2]);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public LocalTime getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalTime orderTime) {
        this.orderTime = orderTime;
    }
}
