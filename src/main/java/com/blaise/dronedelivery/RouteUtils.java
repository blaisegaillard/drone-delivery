package com.blaise.dronedelivery;

import java.time.LocalTime;
import java.util.*;

public class RouteUtils {

    public static Route findOptimalRoute(List<Order> orders) {
        SortedMap<Integer, Route> allRoutes = findAllPossibleRoutes(orders);
        Integer nps = allRoutes.lastKey();
        return allRoutes.get(nps);
    }


    private static SortedMap<Integer, Route> findAllPossibleRoutes(List<Order> orders) {
        return permute(orders, 0, new TreeMap<>());
    }

    private static SortedMap<Integer, Route> permute(List<Order> orders, int start, SortedMap<Integer, Route> result) {

        if (start >= orders.size()) {
            Route route = new Route(orders);
            if (result == null || result.isEmpty() || route.calculateNPS() > result.lastKey()) {
                if (route.getDeliveries().get(route.getDeliveries().size() - 1).getDeliveryTime().isBefore(LocalTime.parse("22:00:00"))) {
                    result.put(route.getNPS(), route);
                }
            }
        }

        for (int j = start; j <= orders.size() - 1; j++) {
            Collections.swap(orders, start, j);
            permute(orders, start + 1, result);
            Collections.swap(orders, start, j);
        }
        return result;
    }
}
