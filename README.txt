Drone Delivery Challenge

Assumptions:

1. Input file is of correct format
2. At least one possible route will end before 10pm
3. Order and delivery will be on the same day
4. Input is of reasonable size (stack overflow is possible because of the factorial scaling and recursive call)

To build:
1. mkdir out (if necessary)
2. javac -d out -sourcepath src src/main/java/com/blaise/dronedelivery/Main.java
3. javac -d out -sourcepath src/test/java/ -cp out:lib/apiguardian-api-1.0.0.jar:lib/junit-jupiter-api-5.3.2.jar:lib/junit-platform-commons-1.3.2.jar:lib/opentest4j-1.1.1.jar:lib/junit-platform-console-standalone-1.3.2.jar src/test/java/com/blaise/dronedelivery/*.java

To package:
1. Go to "out"
2. jar cvfm drone.jar ../src/main/java/META-INF/MANIFEST.MF .

To test:
1. Go to base directory
2. java -jar lib/junit-platform-console-standalone-1.3.2.jar --class-path out --scan-class-path

To run:
1. Go to "out"
2. java -jar drone.jar "../drone_test.txt"